from setuptools import setup, find_packages
setup(
    name="git_tools",
    entry_points={
        "console_scripts": [
            "merge-view = git_tools.merge_view:merge_view_command",
            "squash-msg = git_tools.squash_message:build_squash_message",
        ],
    }
)