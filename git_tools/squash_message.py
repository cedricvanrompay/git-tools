import sys
import subprocess
import traceback
from textwrap import indent
from pathlib import Path

import requests

from .utils import (
    get_cmd_output,
)

def _indent_as_list_item(text: str):
    first_line, rest = text.split('\n', maxsplit=1)

    result = f'- {first_line}'

    rest = rest.strip()
    if rest:
        result += '\n'*2
        result += indent(rest, ' '*4)
        result += '\n'

    return result

def build_squash_message():
    from_branch = sys.argv[1] if len(sys.argv) == 2 else 'master'
    
    try:
        hashes = get_cmd_output(f'git log --format=%h {from_branch}..HEAD').split()
    except subprocess.CalledProcessError as proc_error:
        print(proc_error.stderr)
        traceback.print_exc()
        sys.exit(1)

    parts = (
        _indent_as_list_item(get_cmd_output(f'git log -1 --format=%s%n%n%b {hash}'))
        for hash in reversed(hashes)
    )
    squashed = '\n'.join(parts)

    merge_request_number = get_merge_request_number()

    to_edit = '\n\n'.join([
        '# TODO commit message',
        f'See merge request !{merge_request_number}',
        'messages of squashed commits (oldest first):\n'+squashed,
    ])

    subprocess.run(
        ['vim', '--'],
        input=to_edit.encode()
    )

def get_merge_request_number():
    current_branch = get_cmd_output('git rev-parse --abbrev-ref HEAD').strip()

    with open(Path(__file__).parent.parent/'.gitlab-token') as f:
        token = f.read().strip()

    r = requests.get(
        f'https://gitlab.com/api/v4/merge_requests?source_branch={current_branch}',
        headers={
            'Authorization': f'Bearer {token}'
        },
    )
    r.raise_for_status()

    body = r.json()

    if len(body) == 0:
        raise Exception('No merge requests found')

    # TODO filter by project ID
    # in case several projects have a branch with the same name
    assert len(body) == 1
    merge_request = body[0]
    assert merge_request['state'] == 'opened', f'''status = {merge_request['status']}'''

    merge_request_number = int(merge_request['iid'])
    return merge_request_number
