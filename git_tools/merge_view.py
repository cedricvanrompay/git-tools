import re
import sys

from .utils import (
    run_cmd,
    get_current_branch,
    repository_is_dirty,
    branch_exists,
)

def view_merge_request(mr_target):
    current_branch = get_current_branch()

    if current_branch == None:
        pass
        # raise Exception('no current branch')
    elif current_branch in ['master']:
        raise Exception(f'unapplicable on branch "{current_branch}"')

    if repository_is_dirty():
        raise Exception('repository seems dirty')

    target_branch = f'merge-target-{current_branch}'
    if branch_exists(target_branch):
        raise Exception(f'there is already a branch "{target_branch}"')

    run_cmd('git checkout --detach')
    run_cmd(f'git reset --soft {mr_target}')
    run_cmd(f'git checkout -b {target_branch}')

class NotInMergeView(Exception):
    pass

def quit_merge_view():
    current_branch = get_current_branch()

    if not current_branch:
        raise NotInMergeView

    match = re.match('^merge-target-(.+)$', current_branch)

    if not match:
        raise NotInMergeView

    source_branch = match.group(1)

    run_cmd(f'git checkout {source_branch}')
    run_cmd(f'git branch -d {current_branch}')

def toggle_merge_view(mr_target):
    run_cmd('git fetch')
    try:
        # TODO if we are in merge view we should not allow a "mr_target" parameter
        quit_merge_view()
    except NotInMergeView:
        view_merge_request(mr_target)

def merge_view_command():
    if len(sys.argv) == 2:
        mr_target = sys.argv[1]
    else:
        mr_target = "master"
        
    toggle_merge_view(mr_target)
