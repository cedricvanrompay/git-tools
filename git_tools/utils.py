import subprocess

def run_cmd(cmd, capture_output=True):
    if isinstance(cmd, str):
        cmd = cmd.split()
    subproc = subprocess.run(cmd, capture_output=capture_output)
    subproc.check_returncode()
    return subproc

def get_cmd_output(cmd, check_returncode=True):
    if isinstance(cmd, str):
        cmd = cmd.split()
    subproc = subprocess.run(cmd, capture_output=True)
    if check_returncode:
        subproc.check_returncode()
    return subproc.stdout.decode()

def branch_exists(branch):
    subproc = subprocess.run(f'git show-ref --verify --quiet refs/heads/{branch}'.split())
    if subproc.returncode == 0:
        return True
    else:
        return False

def get_current_branch():
    output_lines = get_cmd_output(
        'git symbolic-ref --short -q HEAD',
        check_returncode=False,
    ).splitlines()
    
    if len(output_lines) == 0:
        return None
    elif len(output_lines) == 1:
        return output_lines[0]
    else:
        raise Exception(
            'unexpected number of output lines:\n'
            + str(output_lines)
        )

def repository_is_dirty():
    if get_cmd_output('git status --porcelain'):
        return True
    else:
        return False
