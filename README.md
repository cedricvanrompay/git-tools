# Git Tools

My own personal git tools.

## Setup

Only requirement is Python 3.

Right now to set it up you should run the following command in this directory:

    pip install -e ./

This will install the package as “external”, which essentially means what will be installed is just a symlink to this directory: modifying the code (or doing `git pull`) will instantly change the behavior of the “installed” program.

## Usage

For now the only thing it does is installing one command: `merge-view`.
It puts you in and out of merge view.